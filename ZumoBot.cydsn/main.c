#include <project.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"
#include "Motor.h"
#include "Ultra.h"
#include "Nunchuk.h"
#include "Reflectance.h"
#include "Gyro.h"
#include "Accel_magnet.h"
#include "LSM303D.h"
#include "IR.h"
#include "Beep.h"
#include "mqtt_sender.h"
#include <time.h>
#include <sys/time.h>
#include "serial1.h"
#include <unistd.h>
#include <stdlib.h>
/*
Jaakko: weekly assignments 1,2,3,7,8,9 and project 2.
Julia: weekly assignments 4,5,6 and project 3. 
Cajus: project 1.
*/
#if 0
    void tankturnl (uint8 l_speed, uint8 r_speed, uint32 delay)
    {SetMotors(1,0, l_speed, r_speed, delay);}
    void tankturnr (uint8 l_speed, uint8 r_speed, uint32 delay)
    {SetMotors(0,1, l_speed, r_speed, delay);}
    
    void zmain(void) {
        struct sensors_ dig;
        srand(time(NULL)); //random is truly random
        Ultra_Start(); //start Ultrasensor
        IR_Start(); //start IR sensor
        IR_flush(); //flush the sensor
        reflectance_start();
        reflectance_set_threshold(9000, 9000, 11000, 11000, 9000, 9000);
        
        int dist = 0;               
        int ajaa = 0;
        int start = 0;
        int stop = 0;
        int time = 0;
        int obstacle = 0;
        const char *topic = "Zumo5/";
        
        motor_start();
        motor_forward(0,0);
        
        while (dig.L3 == 0 && dig.R3 == 0 && ajaa == 0) { //drives to start line
            motor_forward(100,10);
            reflectance_digital(&dig);
            }

        motor_forward(0,0);
        print_mqtt(topic, "ready zumo");
        IR_wait(); // wait for user to send IR
        start = xTaskGetTickCount(); //count since code started
        print_mqtt(topic,"start %d", start);
        motor_forward(100,100); // drive inside the ring
        ajaa = 1; // set to driving in the ring
        BatteryLed_Write(true); // led on
            
        while(ajaa == 1) { // while robot drives in the ring
            motor_forward(200,10); //motor forward while driving and nothing blocking               
            reflectance_digital(&dig); //check digs
            dist = Ultra_GetDistance();
            int random = rand() % (150 - 50 +1) + 50; // get a random number for turns
        
            if (dist <= 5) { // if obstacle is within 5(cm?) from robot makes a random turn to the right
                motor_backward(150,100); //reverse
                tankturnr(random, random, 300); //random turn to the right
                obstacle = xTaskGetTickCount();
                time = obstacle - start; //get correct time for obstacle hit
                print_mqtt(topic,"obstacle %d", time); // print time when hit obstacle
            }
            else if (dig.L3 == 1 || dig.L2 == 1|| dig.L1 == 1) { //if left sensors detect a wall make a random tankturn right
                motor_backward(150,100); //reverse
                tankturnr(random, random, 300); // random turn to the right
             }
            else if (dig.R1 == 1 || dig.R2 == 1 || dig.R3 == 1) { //if right sensors detect a wall make a random tankturn left
                motor_backward(150,100); //reverse
                tankturnl(random, random, 300); //random turn to the left
            }
            else if (SW1_Read() == 0) { // if button is pressed robot stops
                stop = xTaskGetTickCount(); // get count since prog start
                time = stop - start; // time code ran
                print_mqtt(topic,"stop %d", stop);
                print_mqtt(topic,"time %d", time);                   
                motor_forward(0,0);
                motor_stop();
                ajaa = 0;
                BatteryLed_Write(false); // led off
            }
        }
        while (true){ // stops main from ending
        vTaskDelay(100);
        }
    }
#endif // Project 1 by: Cajus

#if 0 // Project2 lil'Botty By: Jaakko
	void prog_Start(uint32_t tDelay), end_Prog();
    void zmain(void){
        struct sensors_ dig;
        prog_Start(10);
        int delay=1, aikaSS=0, aikaLS=0, aikaTS=0, aikaMS=0, aikaLMS=0, aikaSMS=0, misses=1, miss=0, count=0;
        const char * topic = "Zumo5/";
        while (true) {
            motor_forward(255, 0); 
            reflectance_digital( & dig); 
            if (dig.L3 && dig.R3 == 1) {
                count++; 
                if (count == 1) { 
                    reflectance_digital( & dig);
                    motor_forward(0, 0);
                    send_mqtt(topic, "Ready"); 
                    IR_wait();   
                    aikaMS = xTaskGetTickCount(); //Time MS is magic function result.
                    aikaSS = aikaMS / 1000; //MS to S
                    aikaSMS = aikaMS;
                    print_mqtt(topic, "Runstarttime: %ds %dms", aikaSS,aikaMS);
                }else if (count > 2) {
                    reflectance_digital( & dig);
                    aikaMS = xTaskGetTickCount();
                    aikaTS = aikaMS / 1000;
                    send_mqtt(topic, "Stop");
                    print_mqtt(topic, "Endtime: %ds %dms", aikaTS, aikaMS);
                    aikaTS = aikaTS;
                    aikaLS = aikaTS - aikaSS;
                    aikaLMS = aikaMS - aikaSMS;
                    print_mqtt(topic, "Starttime %ds - Endtime %ds = Laptime %ds, %dms", aikaSS, aikaTS, aikaLS, aikaLMS);
                    motor_forward(0, 0);
                    IR_wait();
                } 
                while (dig.L3 == 1 && dig.R3 == 1) { //If sensors L3 and R3 are on black doesnt get stuck when ir send is pressed
                    reflectance_digital( & dig);
                    motor_forward(255, 0);
                }
            }else if(dig.L1 == 1 && dig.R1 == 1 && miss == 0 ){
                print_mqtt(topic, "Line. %dms", aikaMS);
                miss = 1;//Here starts the turning ifs
            }else if (dig.L1 == 0 && dig.R1 == 0) { //Backs up if L1, R1 and sensors are on white and adds a miss to a count
                motor_backward(255, 15);
                if(miss==1){
                    aikaMS = xTaskGetTickCount();
                    print_mqtt(topic, "Lost. %dms %d", aikaMS, misses);
                    misses++;
                    miss = 0;
                }
            }else if (dig.R2 == 1) { //If only R2 is on black turns right
                motor_turn(180, 0, 0);
                vTaskDelay(delay);
                if (dig.R1 == 1) { //If only R1 and R2 and are on black turns right more
                    motor_turn(250, 0, 0);               
                    vTaskDelay(delay);
                }
            }else if (dig.L2 == 1) {
                motor_turn(0, 180, 0);
                vTaskDelay(delay);
                if (dig.L1 == 1) {
                    motor_turn(0, 250, 0);
                    vTaskDelay(delay);
                }
            }
        }   
        end_Prog();
    }
	void prog_Start(uint32_t tDelay) { 
		srand(0);
		motor_start();
		Ultra_Start();
		IR_Start();
		IR_flush();
		reflectance_start();
		reflectance_set_threshold(11000, 13000, 15000, 15000, 13000, 11000);
		motor_forward(0, 0);
		while (SW1_Read() == 1) {
			BatteryLed_Write(true);
			vTaskDelay(tDelay);
			BatteryLed_Write(false);
		}
	}
	void end_Prog() {
		while (true) {
			vTaskDelay(10);
		}
	}
#endif  // Project 2 lil'Botty By: Jaakko

#if 1
    const char* name = "Zumo05/";
    int x = 0, y = -2, d;
    void go(int suunta, int speed, struct sensors_ dig);
    void zmain(void){
        /**Start Prog******************************************************/
        srand(1);
		motor_start();
		Ultra_Start();
		IR_Start();
		IR_flush();
		reflectance_start();
		reflectance_set_threshold(11000, 13000, 15000, 15000, 13000, 11000);
		motor_forward(0, 0);
        
        /**More Variables***************************************************/
        d = Ultra_GetDistance();
        TickType_t start, stop, final;
        int i = 0, max = 10, suunta = 0, osuu = 15;
        unsigned int speed = 250, delay = 290, turnSpeed = 180;
        struct sensors_ dig;
        /**First Line*******************************************************/
        IR_wait();
        while (y < -1){
            motor_forward(speed, 0);
            reflectance_digital(&dig);
            if (dig.L3 == 1 && dig.R3 == 1) {
				y++;
				print_mqtt(name, "position %d %d", x, y);
				while (dig.L3 == 1 && dig.R3 == 1) {
					reflectance_digital(&dig);
					d = Ultra_GetDistance();
    			}
    		}
            motor_forward(0,0);
        }
        print_mqtt(name, "ready maze");
        IR_wait();
        start = xTaskGetTickCount();
        print_mqtt(name, "start %d", start);
        /**Maze*********************************************************/
        while (y < max){//while in the maze
            d = Ultra_GetDistance();
            
            if (d < osuu){//if distance to obstacle is hitting range
                int i = 0;
                
                if (x == 0 || x < 0){ //is centered or left
                    suunta = 1;
                    //turn right
                    for (i = 0; i < 1; i++) {
                        SetMotors(0, 1, turnSpeed, 0, delay);
                    }
                    //go right
                    go(suunta,speed,dig);
                    //turn left
                    for (i = 0; i < 1; i++) {
                        SetMotors(1, 0, 0, turnSpeed, delay);
                    }
                    d = Ultra_GetDistance();
                    while (d < osuu){ //while hits again keep direction
                        suunta = 1;
                        motor_backward(100, 200);
                        motor_forward(0, 0);
                        for (i = 0; i < 1; i++) {
                            SetMotors(0, 1, turnSpeed, 0, delay);
                        }
                        go(suunta,speed,dig);
                        for (i = 0; i < 1; i++) {
                            SetMotors(1, 0, 0, turnSpeed, delay);
                        }
                        d = Ultra_GetDistance();
                    }
                } else { // is right
                    suunta = -1;
                    //turn left
                    for (int i = 0; i < 1; i++) {
                        SetMotors(1, 0, 0, turnSpeed, delay);
                    }
                    go(suunta,speed,dig);
                    //turn right
                    for (int i = 0; i < 1; i++) {
                        SetMotors(0, 1, turnSpeed, 0, delay);
                    }
                    d = Ultra_GetDistance();
                    while (d < osuu){ //while hits again, keep direction
                        suunta = -1;
                        motor_backward(100, 200);
                        motor_forward(0, 0);
                        for (int i = 0; i < 1; i++) {
                            SetMotors(1, 0, 0, turnSpeed, delay);
                        }
                        go(suunta,speed,dig);
                        for (int i = 0; i < 1; i++) {
                            SetMotors(0, 1, turnSpeed, 0, delay);
                        }
                        d = Ultra_GetDistance();
                    }
                }
            } else {
                suunta = 0;
                go(suunta,speed,dig);
                d = Ultra_GetDistance();
            }
        }
        while (x != 0){ //fix x offset
            if (x < 0){ //if too left
                suunta = 1;
                //turn right
                for (i = 0; i < 1; i++) {
                    SetMotors(0, 1, turnSpeed, 0, delay);
                }
                go(suunta, speed, dig); //go right;
                //turn left
                for (i = 0; i < 1; i++) {
                    SetMotors(1, 0, 0, turnSpeed, delay);
                }
            } else { //if too right
                suunta = -1;
                //turn left
                for (i = 0; i < 1; i++) {
                    SetMotors(1, 0, 0, turnSpeed, delay);
                }
                go(suunta, speed, dig); //go left;
                //turn right
                for (i = 0; i < 1; i++) {
                    SetMotors(0, 1, turnSpeed, 0, delay);
                }
            }
        }
        //last line
        reflectance_digital(&dig);
    	while (dig.L1 == 1 || dig.R1 == 1) { //until line ends
            motor_forward(speed, 0);
            reflectance_digital(&dig);
            reflectance_digital(&dig);
    			if (dig.L3 == 1 && dig.R3 == 1) {
    				y++;
    				print_mqtt(name, "position %d %d", x, y);
    				while (dig.L3 == 1 && dig.R3 == 1) {
    					reflectance_digital(&dig);
    					d = Ultra_GetDistance();
    				}
    			}
        }
        /**Stop**************************************************************/
        motor_forward(0,0);
        motor_stop();
        stop = xTaskGetTickCount();
        print_mqtt(name, "stop %d", stop);
        final = stop - start;
        print_mqtt(name, "time %d", final);
        while (true) {
			vTaskDelay(10);
		}
    }
    void go(int suunta, int speed, struct sensors_ dig){
    	int max;
    	if (suunta == 0){ //go forward
    		max = y+1;
            d = Ultra_GetDistance();
    		while ((y < max) && !(d < 10)){
    			motor_forward(speed, 0);
    			d = Ultra_GetDistance();
    			reflectance_digital(&dig);
    			if (dig.L3 == 1 && dig.R3 == 1) {
    				y++;
    				print_mqtt(name, "position %d %d", x, y);
    				while (dig.L3 == 1 && dig.R3 == 1) {
    					reflectance_digital(&dig);
    					d = Ultra_GetDistance();
    				}
    			}
    			motor_forward(0,0);
    		}
    	} else if (suunta == 1){ //go right
    		max = x+1;
            d = Ultra_GetDistance();
    		while (x < max){
    			motor_forward(speed, 0);
    			d = Ultra_GetDistance();
    			reflectance_digital(&dig);
    			if (dig.L3 == 1 && dig.R3 == 1) {
    				x++;
    				print_mqtt(name, "position %d %d", x, y);
    				while (dig.L3 == 1 && dig.R3 == 1) {
    					reflectance_digital(&dig);
    					d = Ultra_GetDistance();
    				}
    			}
    			motor_forward(0,0);
    		}
    	} else if (suunta == -1){ //go left, could also be just else
    		max = x-1;
            d = Ultra_GetDistance();
    		while (x > max){
    			motor_forward(speed, 0);
    			d = Ultra_GetDistance();
    			reflectance_digital(&dig);
    			if (dig.L3 == 1 && dig.R3 == 1) {
    				x--;
    				print_mqtt(name, "position %d %d", x, y);
    				while (dig.L3 == 1 && dig.R3 == 1) {
    					reflectance_digital(&dig);
    					d = Ultra_GetDistance();
    				}
    			}
    			motor_forward(0,0);
    		}
    	}
    }
#endif //Project 3 by: Julia, 4/5 tracks work
